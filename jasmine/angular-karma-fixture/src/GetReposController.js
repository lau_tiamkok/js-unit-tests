app.controller('GetReposController', ['$scope', '$http', function($scope, $http) {

    // passed.
    // $http.get('https://api.github.com/users/angular/repos')
    //   .success(function(data, status, headers, config) {
    //     $scope.valid = true;
    //     $scope.response = data;
    //   })
    //   .error(function(data, status, headers, config) {
    //     $scope.valid = false;
    // });
    //

    // passed.
    // var config = {
    //     headers:{
    //         'Authorization': "Basic " + btoa("splllctre:sp8ctre"),
    //         'Accept': 'application/json;odata=verbose'
    //     }
    // };
    // $http.get("https://api.github.com/user/repos", config).then(function(response) {
    //     $scope.valid = true;
    //     $scope.myData = response.data;
    // });

    // passed.
    // var config = {
    //     headers:{
    //         'Authorization': "Basic " + btoa("splllctre:sp8ctre"),
    //         'Accept': 'application/json;odata=verbose'
    //     }
    // };
    // $http.get("https://api.github.com/user/repos", config)
    // .success(function(data, status, headers, config) {
    //     $scope.valid = true;
    //     $scope.collection = data;
    // })
    // .error(function(data, status, headers, config) {
    //     $scope.error = data;
    // });

    // Passed.
    // $http({
    //     method: 'GET',
    //     url: 'https://api.github.com/user/repos',
    //     headers:{
    //         'Authorization': "Basic " + btoa("splllctre:sp8ctre"),
    //         'Accept': 'application/json; odata=verbose'
    //     }
    // })
    // .success(function(data, status, headers, config) {
    //     $scope.valid = true;
    //     $scope.collection = data;
    // })
    // .error(function(data, status, headers, config) {
    //     $scope.error = data;
    // });

    $scope.submit = function() {
        $scope.collection = '';
        $scope.error = '';
        var encodedString = $scope.username + ':' + $scope.password;
        $http({
            method: 'GET',
            url: 'https://api.github.com/user/repos',
            headers:{
                'Authorization': "Basic " + btoa(encodedString),
                'Accept': 'application/json; odata=verbose',
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        .success(function(data, status, headers, config) {
            $scope.valid = true;
            $scope.collection = data;
            $scope.username = '';
            $scope.password = '';
        })
        .error(function(data, status, headers, config) {
            $scope.error = data;
        })
    };

}]);
