describe('GetReposController Test', function() {

    // Boilerplate starts from here...
    // Load the module with MainController
    beforeEach(module('myApp'));

    var controller, $scope, $http, $httpBackend;

    // inject the $controller and $rootScope services.
    // in the beforeEach block.
    beforeEach(inject(function ($rootScope, $controller, $http, $httpBackend) {

        // Create a new scope that's a child of the $rootScope.
        $scope = $rootScope.$new();

        // Create the controller.
        controller = $controller;
        controller("GetReposController", {$scope, $http, $httpBackend});
    }));
    // ...Boilerplate ends here

    // Our tests will go here.
    it('should demonstrate using when (200 status)', inject(function($http, $httpBackend) {

        var $scope = {};

        /* Code Under Test */
        $scope.submit = function() {
            $scope.collection = '';
            $scope.error = '';
            var encodedString = $scope.username + ':' + $scope.password;
            $http({
                method: 'GET',
                url: 'https://api.github.com/user/repos',
                headers:{
                    'Authorization': "Basic " + btoa("splllctre:sp8ctre"),
                    'Accept': 'application/json; odata=verbose',
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
            .success(function(data, status, headers, config) {
                $scope.valid = true;
                $scope.collection = data;
                $scope.username = '';
                $scope.password = '';
            })
            .error(function(data, status, headers, config) {
                $scope.error = data;
            })
        };
        /* End */

        $scope.submit();
        $httpBackend.whenGET('https://api.github.com/user/repos', undefined, {
            Authorization: "Basic " + btoa("splllctre:sp8ctre"),
            Accept: "application/json;odata=verbose",
            ContentType: "application/x-www-form-urlencoded"
        }).respond(function(){ return [200,{foo: 'bar' }]});

        $httpBackend.flush();

        expect($scope.valid).toBe(true);
        expect($scope.collection).toEqual({ foo: 'bar' });

    }));



    // it('should demonstrate using when (200 status)', inject(function($http, $httpBackend) {

    //     var $scope = {};

    //     /* Code Under Test */
    //     $http({
    //         method: 'GET',
    //         url: 'https://api.github.com/user/repos',
    //         headers:{
    //             'Authorization': "Basic " + btoa("splllctre:sp8ctre"),
    //             'Accept': 'application/json; odata=verbose'
    //         }
    //     })
    //     .success(function(data, status, headers, config) {
    //         $scope.valid = true;
    //         $scope.collection = data;
    //     })
    //     .error(function(data, status, headers, config) {
    //         $scope.error = data;
    //     });
    //     /* End */

    //     $httpBackend.whenGET('https://api.github.com/user/repos', undefined, {
    //         Authorization: "Basic " + btoa("splllctre:sp8ctre"),
    //         Accept: "application/json;odata=verbose"
    //     }).respond(function(){ return [200,{foo: 'bar' }]});

    //     $httpBackend.flush();

    //     expect($scope.valid).toBe(true);
    //     expect($scope.collection).toEqual({ foo: 'bar' });

    // }));



    // it('should demonstrate using when (200 status)', inject(function($http, $httpBackend) {

    //     var $scope = {};

    //     /* Code Under Test */
    //     var config = {
    //         headers:{
    //             'Authorization': "Basic " + btoa("splllctre:sp8ctre"),
    //             'Accept': 'application/json;odata=verbose'
    //         }
    //     };
    //     $http.get("https://api.github.com/user/repos", config)
    //     .success(function(data, status, headers, config) {
    //         $scope.valid = true;
    //         $scope.collection = data;
    //     })
    //     .error(function(data, status, headers, config) {
    //         $scope.error = data;
    //     });
    //     /* End */

    //   $httpBackend
    //     .when('GET', 'https://api.github.com/user/repos', undefined, {
    //         Authorization: "Basic " + btoa("splllctre:sp8ctre"),
    //         Accept: "application/json;odata=verbose"
    //     })
    //     .respond(200, { foo: 'bar' });

    //   $httpBackend.flush();

    //   expect($scope.valid).toBe(true);
    //   expect($scope.collection).toEqual({ foo: 'bar' });

    // }));

    // it('should demonstrate using when (200 status)', inject(function($http, $httpBackend) {

    //     var $scope = {};

    //     /* Code Under Test */
    //     var config = {
    //     headers:{
    //         'Authorization': "Basic " + btoa("splllctre:sp8ctre"),
    //         'Accept': 'application/json;odata=verbose'
    //         }
    //     };
    //     $http.get("https://api.github.com/user/repos", config).then(function(response) {
    //         $scope.valid = true;
    //         $scope.myData = response.data;
    //     });
    //     /* End */

    //   $httpBackend
    //     .when('GET', 'https://api.github.com/user/repos', undefined, {
    //         Authorization: "Basic " + btoa("splllctre:sp8ctre"),
    //         Accept: "application/json;odata=verbose"
    //     })
    //     .respond(200, { foo: 'bar' });

    //   $httpBackend.flush();

    //   expect($scope.valid).toBe(true);
    //   expect($scope.myData).toEqual({ foo: 'bar' });

    // }));

    // it('should demonstrate using when (200 status)', inject(function($http, $httpBackend) {

    //   var $scope = {};

    //   /* Code Under Test */
    //   $http.get('https://api.github.com/users/angular/repos')
    //     .success(function(data, status, headers, config) {
    //       $scope.valid = true;
    //       $scope.response = data;
    //     })
    //     .error(function(data, status, headers, config) {
    //       $scope.valid = false;
    //   });
    //   /* End */

    //   $httpBackend
    //     .when('GET', 'https://api.github.com/users/angular/repos')
    //     .respond(200, { foo: 'bar' });

    //   $httpBackend.flush();

    //   expect($scope.valid).toBe(true);
    //   expect($scope.response).toEqual({ foo: 'bar' });

    // }));

    // Our tests will go here.
    // it("should assign message to hello world", function () {
    //     expect(scope.message).toBe("Hello World");
    // });

    // it('should create $scope.greeting and say \'Hello World\' when calling sayHello method', function() {
    //     expect(scope.greeting).toBeUndefined();
    //     expect(scope.name).toEqual("World");
    //     scope.sayHello();
    //     expect(scope.greeting).toEqual("Hello World");
    // });
});
